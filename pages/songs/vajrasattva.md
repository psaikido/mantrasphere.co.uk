---  
title: Vajrasattva  
layout: default  
---  
  
  
## Vajrasattva  
<img src="/assets/images/vajrasattva.jpg" alt="" />  
This is the 'one hundred syllable' mantra of <a href="http://en.wikipedia.org/wiki/Vajrasattva">Vajrasattva</a>, the 'diamond' being who is the great purifier.
