---      
title: Great Light  
layout: default      
---      
      
## Great Light  
  
there's beings out there  
they've never seen light  
in the voids between the worlds  
they think that they're alone  
they think that they're alone  
oh but if a great light would shine  
they could see their own kind  
oh but if a great light would shine  
the path they'd find  
   
there's beings out there  
who've never known love  
in the voids between the worlds  
they think that they're alone  
they think that they're alone  
but oh if a great light would shine  
they could see they're own kind  
oh if a great light would shine  
the path they'd find  
   
can you hear their hearts  
being time?  
can you see the silence  
shine?  
om mani padme hum  
om mani padme hum  
***  

This song is a response to the part of the the <a href="http://www.mahindarama.com/e-tipitaka/Majjhima-Nikaya/mn-123.htm">Acchariyabbhuta-dhamma Sutta</a> 'Wonderful and Marvellous Sutta', number 123 in the Majjhima Nikaya  
