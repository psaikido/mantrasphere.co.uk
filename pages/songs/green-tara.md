---        
title: Green Tara
layout: default        
---        

## Green Tara

OM TARE TUTARRE TURE SOHA

Mahasukha has done a <a href="http://mahasukha.bandcamp.com/track/green-tara-mantra">cover of this one</a>!

<img src="/assets/images/green_tara.jpg" alt="" width="299" height="322" />  

Green Tara is related to Chenrezig in the last song.

As Avalokiteshvara realises how big his vow is and how long it is going to take, he cries a big tear.  As it lands on the ground it is transformed into Green Tara, the 'swift one', the 'Liberator'.  She is the archetype of active mercy.  Notice how she has one leg coming out of the meditation posture as though she is about to get up.  She's ready to help.
