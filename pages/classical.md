---
title: Classical
layout: default
---

[6 classical pieces from the canon](http://soundcloud.com/mantrasphere/sets/6-pieces-from-the-canon)  
recorded in 1996 or 1997 by Bill King

[5 pieces by Hughie](http://soundcloud.com/mantrasphere/sets/classical)  
written mid 1990s and recorded by Bill King
