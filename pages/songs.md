---
title: Songs
layout: default
---

[01 Padmasambhava](songs/padmasambhava.html)  
[02 Shine](songs/shine.html)  
[03 Blue](songs/blue.html)  
[04 Vajrasattva](songs/vajrasattva.html)  
[05 Three Refuges](songs/three-refuges.html)  
[06 White Tara (E)](songs/white-tara-e.html)  
[07 Great Light](songs/great-light.html)  
[08 White Tara (D)](songs/white-tara-d.html)  
[09 Mani](songs/mani.html)  
[10 Green Tara](songs/green-tara.html)  
[11 The Sailing Stars](songs/sailing-stars.html)  
[12 Tonglen](songs/tonglen.html)  

### other songs
[Everybody's Got To Die](songs/gotta-die.html)  
[Secret Sun](https://m.soundcloud.com/mantrasphere/secret-sun)  
