---
title: Home
layout: default
---

The music of <a href="https://carrollonline.uk">Hugh Carroll</a> 
springs from Buddhist meditation with the 
<a href="http://westernchanfellowship.org">Western Chan Fellowship</a>


<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="89NJ993PW445W">
<input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
</form>


### youtube:
<a title="Padmasambhava video" href="http://www.youtube.com/watch?v=CfmXj1XuYxQ" target="_blank"> 
    <img src="assets/images/gallery/promo/padmasambhava-video.jpg" alt="video" width="200" height="94" />
</a>

<a title="The Three Refuges" href="http://youtu.be/LKj1byuQp3c">Three Refuges</a>

Interview on [Swindon105.5](http://www.swindon1055.com/) with Lisa Coleman  
[one](/assets/mp3/art2art-20110121-1.mp3)
[two](/assets/mp3/art2art-20110121-2.mp3)
[three](/assets/mp3/art2art-20110121-3.mp3)

Produced by Barry Andrews of <a href="http://shriekback.com"><img style="border: none;" src="/assets/images/title2.gif" alt="shriekback" width="150" /></a>

"These mantras, beautifully rendered in a contemporary fashion that retains the spirit of the originals, provide  a profound, musical introduction to the basics of Tibetan tantra and to heartfelt Buddhism in general. The singer-composer is to be warmly congratulated on this innovation."
<em>John Crook , Chuan deng Jing Di. Teacher: Western Chan Fellowship.</em>

